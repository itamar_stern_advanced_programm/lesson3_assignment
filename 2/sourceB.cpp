#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

unordered_map<string, vector<string>> results;
void clearTable ()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

void printTable ()
{
	auto iter = results.end();
	iter--;
	//	int size = results.begin()->second.size();
	int size = iter->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i == -2)
			{
				//cout << it->first << " ";
				printf("|%*s|", 13, it->first.c_str());
			}
			else if (i == -1)
				cout << "_______________";
			else
				//	cout << it->second.at(i) << " ";
				printf("|%*s|", 13, it->second.at(i).c_str());
		}
		cout << endl;
	}
}

int callback (void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

int getLastId (void* notUsed, int argc, char** argv, char** azCol)
{
	cout << "the Last id is: " << argv[0] << endl;
	return 0;
}

/*

	function that recieve an id of a buyer and an id of a car,
	and check if the buyer can buy the car (car available, enough money), and if yes, make the purchase

	INPUT: the buyer id, the car id, the database to check in, place holder for the error (if any)
	OUTPUT: bool, if the purchase was made
*/

bool carPurchase (int buyerid, int carid, sqlite3* db, char* zErrMsg)
{

	int rc = 0;
	int carPrice = 0;
	int accountBalace = 0;

	// get the available parameter adn the price of the car with the given id
	rc = sqlite3_exec(db, string(string("SELECT available,price FROM cars WHERE id = ") + to_string(carid) + ";").c_str(), callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else
	{

		// check if the car is available
		if (results["available"][0]._Equal("1"))
		{

			// set the value of 'carPrice' to the car price
			carPrice = stoi(results["price"][0]);
		}
		else
		{

			cout << "car not available" << endl;
			
			clearTable();

			return false;
		}

		clearTable();
	}

	// get the balance of the account with the given buyer id
	rc = sqlite3_exec(db, string(string("SELECT balance FROM accounts WHERE Buyer_id = ") + to_string(buyerid) + ";").c_str(), callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else
	{

		// set the value of 'accountBalance' to the balance of the account
		accountBalace = stoi(results["balance"][0]);

		if (accountBalace >= carPrice)
		{

			// update the value of 'avaliable' in the table 'cars' to 0 (car not available) in the given car id
			rc = sqlite3_exec(db, string(string("UPDATE cars SET available = 0 WHERE id = ") + to_string(carid) + ";").c_str(), callback, 0, &zErrMsg); //set the car 'available' field to 0 (false)
			
			// set the value of 'balance' in the table 'accounts' tot the previous balance - the car price (make the purchase)
			rc = sqlite3_exec(db, string(string("UPDATE accounts SET balance = ") + to_string(accountBalace - carPrice) + " WHERE Buyer_id = " + to_string(buyerid) + ";").c_str(), callback, 0, &zErrMsg); //"remove" the car price from the account balance
		}
		else
		{

			clearTable();

			cout << "account balance too low" << endl;

			return false;
		}

		clearTable();

		return true;
	}
}

/*

	a function that recieve two accounts id's (from, to) and an amount to transfer, checks if the first account has enough money,
	and if he has, transfer the given amount from the first account to the second

	INPUT: the first account id (from), the second account id (to), the amount to transfer, the database to check in, place holder for the error (if any)
	OUTPUT: bool, if the transfer was made
*/

bool balanceTransfer (int from, int to, int amount, sqlite3* db, char* zErrMsg)
{

	int rc = 0;
	int fromBalance = 0;
	int toBalance = 0;

	// get the balance of the first account (from)
	rc = sqlite3_exec(db, string(string("SELECT balance FROM accounts WHERE id = ") + to_string(from) + ";").c_str(), callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else
	{

		// set the value of 'fromBalance' to the balance of the first account (from)
		fromBalance = stoi(results["balance"][0]);
		clearTable();

		if (fromBalance >= amount) //check if the first account (from) has enough money to make the transfer
		{

			// get the balance of the second account (to)
			rc = sqlite3_exec(db, string(string("SELECT balance FROM accounts WHERE id =  ") + to_string(to) + ";").c_str(), callback, 0, &zErrMsg);
			
			if (rc != SQLITE_OK)
			{
				cout << "SQL error: " << zErrMsg << endl;
				sqlite3_free(zErrMsg);
				system("Pause");
				return 1;
			}
			else
			{

				// set the value of 'toBalance' to the balance of the second account (to)
				toBalance = stoi(results["balance"][0]);
				clearTable();

				// add to the value of 'toBalance' the ampunt to transfer
				toBalance += amount;

				// remove from the value of 'fromBalance' the amount to transfer
				fromBalance -= amount;

				// change the value of column 'balance' in table 'accounts' to the current (after transfer) balance of the first account(from). (at the given from id)
				rc = sqlite3_exec(db, string(string("UPDATE accounts SET balance = ") + to_string(fromBalance) + " WHERE id = " + to_string(from) + ";").c_str(), callback, 0, &zErrMsg);

				// change the value of column 'balance' in table 'accounts' to the current (after transfer) balance of the second account(to). (at the given to id)
				rc = sqlite3_exec(db, string(string("UPDATE accounts SET balance = ") + to_string(toBalance) + " WHERE id = " + to_string(to) + ";").c_str(), callback, 0, &zErrMsg);

				clearTable();
				return true;
			}
		}
		else
		{

			cout << "the balance of the account that send money is too low" << endl;
			return false;
		}

		return true;
	}
}

/*

	a function that recieve an id of a car, and print every accounts that can buy her (has balance higher/equal to the car price)

	INPUT: the car id, the database to check in, place holder for the error (if any)
	OUTPUT: None
*/
void whoCanBuy (int carid, sqlite3* db, char* zErrMsg)
{

	int rc = 0;
	int carPrice = 0;

	// get the price of the car with the given car id
	rc = sqlite3_exec(db, string(string("SELECT price FROM cars WHERE id = ") + to_string(carid) + ";").c_str(), callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return;
	}
	else
	{

		// set the value of 'carPrice' to the price of the car with the given id
		carPrice = stoi(results["price"][0]);
		clearTable();

		// get every account that has balance higher/equal to the car price
		rc = sqlite3_exec(db, string(string("SELECT * FROM accounts WHERE balance >= ") + to_string(carPrice) + ";").c_str(), callback, 0, &zErrMsg);

		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
		}
		else
		{

			// print the accounts
			printTable();
			clearTable();
			return;
		}
	}
}

int main ()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;

	// connection to the database
	rc = sqlite3_open("carsDealer.db", &db);

	if (rc)
	{
		sqlite3_close(db);
		system("PAUSE");
		return 1;
	}

	system("CLS");
	clearTable();

	if (true) // PART A
	{

		bool res = true;

		////////////////////////// success

		res = carPurchase(9, 3, db, zErrMsg);
		if (res) cout << "purchase was made" << endl;

		////////////////////////// success

		res = carPurchase(3, 12, db, zErrMsg);
		if (res) cout << "purchase was made" << endl;

		////////////////////////// fail

		res = carPurchase(3, 1, db, zErrMsg);
		if (res) cout << "purchase was made" << endl;
	}

	//////////////////

	system("pause");

	sqlite3_close(db);

}